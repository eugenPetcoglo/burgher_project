import * as dotenv from 'dotenv'

dotenv.config();


const config = {
    port: process.env.PORT,
    db: {
        host: process.env.POSTGRES_HOST,
        port: +process.env.POSTGRES_PORT,
        username: process.env.POSTGRES_USERNAME,
        password: process.env.POSTGRES_PASSWORD,
        database: process.env.POSTGRES_DATABASE,
    },
    auth_service: {
        your_secret_key: process.env.SECRET_KEY,
        your_refresh_secret_key: process.env.REFRESH_SECRET_KEY,
        expire_time: process.env.EXPIRE_TIME,
        refresh_time: process.env.REFRESH_EXPIRATION,
}
};


export default config;
