import {Request, Response} from "express";
import {validate} from "class-validator";
import {RegisterDto} from "../dto/register-dto";
import {LoginDto} from "../dto/login-dto";
import {AuthService} from "../services/auth-service";
import {plainToInstance} from "class-transformer";
import {RefreshTokenDto} from "../dto/refreshToken-dto";



export class AuthController {
    private authService: AuthService;
    constructor() {
      this.authService = new AuthService();
    }
public registerUser = async (req: Request, res:Response): Promise<void> => {
    try {
        const dto: RegisterDto = plainToInstance(RegisterDto, req.body);
        const errors = await validate(dto);
        if (errors.length > 0) {
            res.status(400).json({errors: errors});
            return;
        }
        await this.authService.registerUser(dto);
        res.status(201).json({message: 'User registred succesfully'});

    } catch (error) {
        res.status(400).json({error: error.message});
    }
};
public loginUser = async (req: Request, res: Response): Promise<void> => {
    try {
        const dto: LoginDto = plainToInstance(LoginDto, req.body);
    const errors = await validate(dto);
    if (errors.length > 0){
        res.status(400).json({ errors: errors});
        return;
    }

        const tokens = await this.authService.loginUser(dto);
        res.status(200).json(tokens);
    } catch (error) {
        res.status(401).json({error: error.message});
    }
};
    public getRefreshedTokens = async (req: Request, res: Response): Promise<void> => {
        try {
            const dto: RefreshTokenDto = plainToInstance(RefreshTokenDto, req.body);
            const errors = await validate(dto)
            if (errors.length > 0) {
                res.status(400).json({ errors: errors});
                return;
            }
            const tokens = await this.authService.refreshToken(dto);
            res.status(200).json(tokens);
        } catch (error) {
            res.status(400).json({ error: error.message });
        }
    };


}
