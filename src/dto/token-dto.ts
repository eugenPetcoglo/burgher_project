import {IsNotEmpty} from "class-validator";

export class TokenDto {
    @IsNotEmpty()
    accessToken: string;

    @IsNotEmpty()
    refreshToken: string;
}
