import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn
} from "typeorm";
import {BurgerEntity} from "./burger.entity";
import {IngredientEntity} from "./ingredient.entity";

@Entity({name: 'burger_ingredient'})
    export class BurgerIngredientEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    order: number

    @Column({name: "ingredient_id" })
    ingredientId: string

    @Column({name: "burger_id" })
    burgerId: string

    @CreateDateColumn({name: "created_at"})
    createdAt: Date

    @UpdateDateColumn({name: "update_at"})
    updateAt: Date

    @ManyToOne(() => BurgerEntity, (burgerEntity) => burgerEntity.burgerIngredientEntities)
    @JoinColumn({name: "burger_id"})
    burgerEntity: BurgerEntity

    @ManyToOne(() => IngredientEntity, (ingredientEntity) => ingredientEntity.burgerIngredientEntities)
    @JoinColumn({name: "ingredient_id"})
    ingredientEntity: IngredientEntity

}
