import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany} from "typeorm"
import {BurgerIngredientEntity} from "./burger-ingredient.entity";
import {PaymentBurgerEntity} from "./payment-burger.entity";

@Entity({name: 'burger'})
    export class BurgerEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    name: string

    @Column()
    description: string

    @Column()
    price: number

    @Column()
    grams: number

    @Column()
    url: string

    @CreateDateColumn({name: "created_at"})
    createdAt: Date

    @UpdateDateColumn({name: "update_at"})
    updateAt: Date

    @OneToMany(() => BurgerIngredientEntity, (burgerIngredientEntity) => burgerIngredientEntity.burgerEntity)
    burgerIngredientEntities: BurgerIngredientEntity[]

    @OneToMany(() => PaymentBurgerEntity, (paymentBurgerEntity) => paymentBurgerEntity.burgerEntity)
    paymentBurgerEntities: PaymentBurgerEntity[]

}
