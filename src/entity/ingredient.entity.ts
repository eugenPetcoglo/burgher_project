import {Entity, PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn, OneToMany} from "typeorm"
import {BurgerIngredientEntity} from "./burger-ingredient.entity";
import {PaymentBurgerIngredientEntity} from "./payment-burger-ingredient.entity";

@Entity({name: 'ingredient'})
    export class IngredientEntity {

    @PrimaryGeneratedColumn("uuid")
    id: string

    @Column()
    name: string

    @Column()
    category: string

    @Column()
    price: number

    @Column()
    grams: number

    @Column()
    url: string

    @CreateDateColumn({name: "created_at"})
    createdAt: Date

    @UpdateDateColumn({name: "update_at"})
    updateAt: Date

    @OneToMany(() => BurgerIngredientEntity, (burgerIngredientEntity) => burgerIngredientEntity.ingredientEntity)
    burgerIngredientEntities: BurgerIngredientEntity[]

    @OneToMany(() => PaymentBurgerIngredientEntity, (paymentBurgerIngredientEntity) => paymentBurgerIngredientEntity.ingredientEntity)
    paymentBurgerIngredientEntities: PaymentBurgerIngredientEntity[]

}
