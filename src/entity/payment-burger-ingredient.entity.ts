import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn
} from "typeorm";
import {IngredientEntity} from "./ingredient.entity";
import {PaymentBurgerEntity} from "./payment-burger.entity";

@Entity({name: 'payment_burger_ingredient'})
    export class PaymentBurgerIngredientEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    order: number

    @Column({name: "ingredient_id"})
    ingredientId: string

    @Column({name: "payment_burger_id"})
    paymentBurgerId: string

    @CreateDateColumn({name: "created_at"})
    createdAt: Date

    @UpdateDateColumn({name: "update_at"})
    updateAt: Date

    @ManyToOne(() => IngredientEntity, (ingredientEntity) => ingredientEntity.paymentBurgerIngredientEntities)
    @JoinColumn({name: "ingredient_id"})
    ingredientEntity: IngredientEntity

    @ManyToOne(() => PaymentBurgerEntity, (paymentBurgerEntity) => paymentBurgerEntity.paymentBurgerIngredientEntities)
    @JoinColumn({name: "payment_burger_id" })
    paymentBurgerEntity: PaymentBurgerEntity

}
