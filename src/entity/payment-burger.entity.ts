import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn, OneToMany
} from "typeorm";
import {BurgerEntity} from "./burger.entity";
import {PaymentBurgerIngredientEntity} from "./payment-burger-ingredient.entity";
import {PaymentEntity} from "./payment.entity";

@Entity({name: 'payment_burger'})
    export class PaymentBurgerEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    name: string

    @Column()
    price: number

    @Column()
    quantity: number

    @Column()
    grams: number

    @Column({name: "burger_id"})
    BurgerId: string

    @Column({name: "payment_id"})
    paymentId: string

    @CreateDateColumn({name: "created_at"})
    createdAt: Date

    @UpdateDateColumn({name: "update_at"})
    updateAt: Date

    @ManyToOne (() => BurgerEntity, (burgerEntity) => burgerEntity.paymentBurgerEntities)
    @JoinColumn({name: "burger_id"})
    burgerEntity: BurgerEntity

    @OneToMany(() => PaymentBurgerIngredientEntity, (paymentBurgerIngredientEntity) => paymentBurgerIngredientEntity.paymentBurgerEntity)
    paymentBurgerIngredientEntities: PaymentBurgerIngredientEntity[]

    @ManyToOne(() => PaymentEntity, (paymentEntity) => paymentEntity.paymentBurgerEntities)
    @JoinColumn({name: "payment_id"})
    paymentEntity: PaymentEntity

}
