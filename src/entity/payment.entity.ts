import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    OneToMany,
    JoinColumn
} from "typeorm";
import {UserEntity} from "./user.entity";
import {PaymentBurgerEntity} from "./payment-burger.entity";

@Entity({name: 'payment'})
    export class PaymentEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    email: string

    @Column()
    phone: number

    @Column()
    status: string

    @Column({name: "user_id"})
    userId: string

    @CreateDateColumn({name: "created_at"})
    createdAt: Date

    @UpdateDateColumn({name: "update_at"})
    updateAt: Date

    @ManyToOne(() => UserEntity, (userEntity) => userEntity.paymentEntities)
    @JoinColumn({name: "user_id"})
    userEntity: UserEntity

    @OneToMany(() => PaymentBurgerEntity, (paymentBurgerEntity) => paymentBurgerEntity.paymentEntity)
    paymentBurgerEntities: PaymentBurgerEntity[]
}
