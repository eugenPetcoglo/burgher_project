import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    BaseEntity
} from "typeorm";
import {PaymentEntity} from "./payment.entity";

@Entity({name: 'user'})
export class UserEntity{

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    username: string

    @Column()
    email: string

    @Column()
    password: string

    @Column()
    role: string

    @CreateDateColumn({name: "created_at"})
    createdAt: Date

    @UpdateDateColumn({name: "update_at"})
    updateAt: Date

    @OneToMany(() => PaymentEntity, (paymentEntity) => paymentEntity.userEntity)
    paymentEntities: PaymentEntity

}
