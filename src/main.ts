import config from "./config";
import express = require('express')
import {AppDataSource} from './data-source'
import {router} from "./routes/auth-routes";4
import bodyParser from 'body-parser';

import swaggerUi from 'swagger-ui-express'
import YAML from 'yamljs'

const swaggerDocument = YAML.load('./src/swagger/swagger.yaml')

const app = express();

AppDataSource.initialize()
app.use(bodyParser.json());
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use("/auth", router);
app.listen(config.port, () => console.log(`Server running in ${config.db.database} mode on port ${config.port}
Swagger OpenAPI running on http://localhost:3000/api-docs/
`));



