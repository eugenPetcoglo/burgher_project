import {Request, Response, NextFunction} from "express";
import jwt from "jsonwebtoken";
import config from "../config";


export const authenticateToken = (req: Request, res: Response, next: NextFunction) => {
    try {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
        return res.status(401).json({error: "Unauthorized"})
    }

    const token = authHeader && authHeader.split(' ')[1];
    if (!token) {
        return res.status(401).json({error: 'Unauthorized'});
    }


    const decodedUser =jwt.verify(token, config.auth_service.your_secret_key )
    if (!decodedUser) {
        return res.status(401).json({error: 'Unauthorized'});
    }
    req["currentUser"] = decodedUser
    next();
} catch (error) {
        console.error("JWT verification failed", error);
        return res.status(401).json({error: 'Unauthorized'})
    }
}
