import { QueryRunner } from "typeorm";

export class InitTables1707995247286 {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        CREATE TABLE "burger" (
    "id" uuid NOT NULL DEFAULT uuid_generate_v4 (),
    "name" VARCHAR (255),
    "description" VARCHAR (255),
    "price" INT,
    "grams" INT,
    "url" VARCHAR (255),
    "created_at" TIMESTAMP NOT NULL DEFAULT now(),
    "update_at" TIMESTAMP NOT NULL DEFAULT now(),
    PRIMARY KEY ("id")     
        )`);

        await queryRunner.query(`
        CREATE TABLE "burger_ingredient" (
    "id" uuid NOT NULL DEFAULT uuid_generate_v4 (),
    "order" INT,
    "created_at" TIMESTAMP NOT NULL DEFAULT now(),
    "update_at" TIMESTAMP NOT NULL DEFAULT now(),
    "burger_id" uuid,
    "ingredient_id" uuid,
    PRIMARY KEY ("id")
        )`);

        await queryRunner.query(`
        CREATE TABLE "ingredient" (
    "id" uuid NOT NULL UNIQUE DEFAULT uuid_generate_v4 (),
    "name" VARCHAR (255),
    "category" VARCHAR (255),
    "price" INT,
    "grams" INT,
    "url" VARCHAR (255),
    "created_at" TIMESTAMP NOT NULL DEFAULT now(),
    "update_at" TIMESTAMP NOT NULL DEFAULT now(),
    PRIMARY KEY ("id")    
        )`);

        await queryRunner.query(`
        CREATE TABLE "payment" (
    "id" uuid NOT NULL DEFAULT uuid_generate_v4 (),
    "email" VARCHAR (255),
    "phone" INT,
    "status" VARCHAR (255),
    "user_id" uuid,
    "created_at" TIMESTAMP NOT NULL DEFAULT now(),
    "update_at" TIMESTAMP NOT NULL DEFAULT now(),
    PRIMARY KEY ("id")
          
        )`);

        await queryRunner.query(`
        CREATE TABLE "payment_burger" (
    "id" uuid NOT NULL DEFAULT uuid_generate_v4 (),
    "name" VARCHAR (255),
    "price" INT,
    "quantity" INT,
    "grams" INT,
    "burger_id" uuid,
    "payment_id" uuid,
    "created_at" TIMESTAMP NOT NULL DEFAULT now(),
    "update_at" TIMESTAMP NOT NULL DEFAULT now(),
    PRIMARY KEY ("id")
           )`);

        await queryRunner.query(`
        CREATE TABLE "payment_burger_ingredient" (
    "id" uuid NOT NULL DEFAULT uuid_generate_v4 (),
    "order" INT,
    "ingredient_id" uuid,
    "payment_burger_id" uuid,
    "created_at" TIMESTAMP NOT NULL DEFAULT now(),
    "update_at" TIMESTAMP NOT NULL DEFAULT now(),
    PRIMARY KEY ("id")
     
        )`);

        await queryRunner.query(`
        CREATE TABLE "user" (
    "id" uuid NOT NULL DEFAULT uuid_generate_v4 (),
    "username" VARCHAR (255),
    "email" VARCHAR (255),
    "password" VARCHAR (255),
    "role" VARCHAR (255),
    "created_at" TIMESTAMP NOT NULL DEFAULT now(),
    "update_at" TIMESTAMP NOT NULL DEFAULT now(),
    PRIMARY KEY ("id")      
        )`);

        await queryRunner.query(`    
    ALTER TABLE "burger_ingredient" 
    ADD CONSTRAINT "fk_burger_ingredient_burger" FOREIGN KEY ("burger_id") REFERENCES "burger"("id") `);

        await queryRunner.query(`    
    ALTER TABLE "burger_ingredient" 
    ADD CONSTRAINT "fk_burger_ingredient_ingredient" FOREIGN KEY ("ingredient_id") REFERENCES "ingredient"("id") `);

        await queryRunner.query(`    
    ALTER TABLE "payment_burger" 
    ADD CONSTRAINT "fk_payment_burger_burger" FOREIGN KEY ("burger_id") REFERENCES "burger"("id") `);

        await queryRunner.query(`    
    ALTER TABLE "payment_burger" 
    ADD CONSTRAINT "fk_payment_burger_payment" FOREIGN KEY ("payment_id") REFERENCES "payment"("id") `);

        await queryRunner.query(`    
    ALTER TABLE "payment_burger_ingredient" 
    ADD CONSTRAINT "fk_payment_burger_ingredient_ingredient" FOREIGN KEY ("ingredient_id") REFERENCES "ingredient"("id") `);

        await queryRunner.query(`    
    ALTER TABLE "payment_burger_ingredient" 
    ADD CONSTRAINT "fk_payment_burger_ingredient_payment_burger" FOREIGN KEY ("payment_burger_id") REFERENCES "payment_burger"("id") `);

        await queryRunner.query(`    
    ALTER TABLE "payment" 
    ADD CONSTRAINT "fk_payment_user" FOREIGN KEY ("user_id") REFERENCES "user"("id") `);



    }

    public async down(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.query(`ALTER TABLE "burger_ingredient" DROP CONSTRAINT "fk_burger_ingredient_burger" `);
        await queryRunner.query(`ALTER TABLE "burger_ingredient" DROP CONSTRAINT "fk_burger_ingredient_ingredient" `);
        await queryRunner.query(`ALTER TABLE "payment_burger" DROP CONSTRAINT "fk_payment_burger_burger" `);
        await queryRunner.query(`ALTER TABLE "payment_burger" DROP CONSTRAINT "fk_payment_burger_payment" `);
        await queryRunner.query(`ALTER TABLE "payment_burger_ingredient" DROP CONSTRAINT "fk_payment_burger_ingredient_ingredient" `);
        await queryRunner.query(`ALTER TABLE "payment_burger_ingredient" DROP CONSTRAINT "fk_payment_burger_ingredient_payment_burger" `);
        await queryRunner.query(`ALTER TABLE "payment" DROP CONSTRAINT "fk_payment_user" `);

        await queryRunner.query(`DROP TABLE "payment_burger_ingredient" CASCADE`);
        await queryRunner.query(`DROP TABLE "payment_burger" CASCADE`);
        await queryRunner.query(`DROP TABLE "burger_ingredient" CASCADE`);
        await queryRunner.query(`DROP TABLE "burger" CASCADE`);
        await queryRunner.query(`DROP TABLE "ingredient" CASCADE`);
        await queryRunner.query(`DROP TABLE "payment" CASCADE`);
        await queryRunner.query(`DROP TABLE "user" CASCADE`);



    }

}
