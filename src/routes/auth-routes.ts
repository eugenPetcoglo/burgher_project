import express = require('express')
import {AuthController} from "../controllers/auth-controller";


export const router = express.Router();
const authController = new AuthController()



router.post('/register' ,authController.registerUser);

router.post('/login', authController.loginUser);

router.post('/refresh-token', authController.getRefreshedTokens)

