import {AppDataSource} from "../data-source";
import {UserEntity} from "../entity/user.entity";
import {RegisterDto} from "../dto/register-dto";
import {LoginDto} from "../dto/login-dto";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import config from "../config";
import {TokenDto} from "../dto/token-dto";
import {plainToInstance} from "class-transformer";
import {RefreshTokenDto} from "../dto/refreshToken-dto"

export class AuthService {
    async registerUser(dto: RegisterDto): Promise<void> {
        const userRepository = AppDataSource.getRepository(UserEntity);

        const existingUser = await userRepository.findOne({where: {email: dto.email, username: dto.username}});
        if (existingUser) {
            throw new Error('User with this email already exists');
        }

        const hashedPassword = await bcrypt.hash(dto.password, 10);
        const newUser = userRepository.create({
            email: dto.email,
            username: dto.username,
            password: hashedPassword,
        });

        await userRepository.save(newUser);

    }

    async loginUser(dto: LoginDto): Promise<TokenDto> {
        const userRepositoy = AppDataSource.getRepository(UserEntity);
        const user = await userRepositoy.findOne({where: {email: dto.email}});

        if (!user) {
            throw new Error('User not found');
        }
        const passwordMatch = await bcrypt.compare(dto.password, user.password);

        if (!passwordMatch) {
            throw new Error('Invalid password');
        }
        const accessToken = jwt.sign({userId: user.id, email: user.email}, config.auth_service.your_secret_key, {expiresIn: config.auth_service.expire_time});
        const refreshToken = jwt.sign({userId: user.id, email: user.email}, config.auth_service.your_refresh_secret_key, {expiresIn: config.auth_service.refresh_time});
        return plainToInstance(TokenDto, {
            accessToken,
            refreshToken
        } satisfies  TokenDto)
    }

    async refreshToken(dto: RefreshTokenDto): Promise<TokenDto> {

        let decodedRefreshToken;
        try {
            decodedRefreshToken = jwt.verify(dto.refreshToken, config.auth_service.your_refresh_secret_key);
        } catch (error) {
            throw new Error('Invalid refresh token');
        }

        const accessToken = jwt.sign({ userId: decodedRefreshToken.userId, email: decodedRefreshToken.email }, config.auth_service.your_secret_key, { expiresIn: config.auth_service.expire_time });
        const newRefreshToken = jwt.sign({ userId: decodedRefreshToken.userId, email: decodedRefreshToken.email }, config.auth_service.your_refresh_secret_key, { expiresIn: config.auth_service.refresh_time });

        return plainToInstance(TokenDto, {
            accessToken,
            refreshToken: newRefreshToken
        });
    }


}
